require "#{Rails.root}/app/helpers/application_helper"
include ApplicationHelper

namespace :yl do
	desc "Checks connectivity to the YL API"
	task :version => :environment do
	  api_version
	end
end
