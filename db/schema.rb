# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130504050059) do

  create_table "bm_bases", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bm_companies", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bm_resources", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "boat_pictures", :force => true do |t|
    t.integer "boat_id"
    t.string  "url"
    t.string  "width"
    t.string  "height"
    t.string  "description"
  end

  create_table "boat_prices", :force => true do |t|
    t.integer "boat_id"
    t.string  "boat_guid"
    t.float   "period_price"
    t.integer "period_nights",     :default => 0
    t.string  "period_start_date"
    t.string  "period_end_date"
    t.string  "calc"
  end

  create_table "sunsail_boats", :force => true do |t|
    t.string  "supplier_guid"
    t.string  "name"
    t.string  "model_name"
    t.string  "model_year"
    t.text    "description"
    t.string  "hull"
    t.integer "length_ft"
    t.string  "cabins"
    t.string  "berths"
    t.string  "heads"
    t.integer "max_persons"
    t.integer "fuel_capacity"
    t.integer "water_capacity"
  end

  create_table "supplier_marinas", :force => true do |t|
    t.string "supplier_guid"
    t.string "name"
    t.float  "longitude"
    t.float  "latitude"
    t.string "address"
  end

end
