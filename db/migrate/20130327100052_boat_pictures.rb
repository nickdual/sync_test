class BoatPictures < ActiveRecord::Migration
  def up
    create_table :boat_pictures do |t|
      t.integer :boat_id
      t.string :url
      t.string :width
      t.string :height
      t.string :description
    end
  end

  def down
  end
end
