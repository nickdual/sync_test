class CreateBoatPrices < ActiveRecord::Migration
  def up
    create_table :boat_prices do |t|
      t.integer :boat_id
      t.string :boat_guid
      t.float :period_price
      t.integer :period_nights, :default => 0
      t.string :period_start_date
      t.string :period_end_date
      t.string :calc
    end
  end

  def down
  end
end
