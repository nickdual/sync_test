class CreateSunsailBoats < ActiveRecord::Migration
  def up
    create_table :sunsail_boats do |t|
      t.string :supplier_guid
      t.string :name
      t.string :model_name
      t.string :model_year
      t.text :description
      t.string :hull
      t.integer :length_ft
      t.string :cabins
      t.string :berths
      t.string :heads
      t.integer :max_persons
      t.integer :fuel_capacity
      t.integer :water_capacity
    end
  end

  def down
  end
end
