class CreateSupplierMarinas < ActiveRecord::Migration
  def up
    create_table :supplier_marinas do |t|
      t.string :supplier_guid
      t.string :name
      t.float :longitude
      t.float :latitude
      t.string :address
    end
  end

  def down
  end
end
