# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

10.times do |n|
  SupplierMarina.create({:supplier_guid => "MKLF" + rand(5..30).to_s, :name => "Milna / ACI Milna", :longitude => 1.23424, :latitude => 143.5433})
end