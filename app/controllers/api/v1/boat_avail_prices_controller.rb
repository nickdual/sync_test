class Api::V1::BoatAvailPricesController < ApplicationController
  def list
    api_key = request.headers['API-KEY']
    if api_key == APP_CONFIG['yl_client']['api_key']
      prices = params['boat_guid'].blank? ? BoatPrice.all : BoatPrice.where(:boat_guid => params['boat_guid'])
      datum = []
      prices.each do |price|
        item = nil
        datum.each do |data|
          item = data if data[:boat_id] == price.boat_id
        end
        item.blank? ? datum.push({:boat_id => price.boat_id, :boat_guid => price.boat_guid, :boat_avail_prices => [price.attributes]}): item[:boat_avail_prices].push(price.attributes)
      end
      respond_to do |format|
        format.json { render :json => {:boats => datum}, :status => 200 }
      end
    else
      respond_to do |format|
        format.json { render :json => {}, :status => 401 }
      end
    end
  end

  def create
    api_key = request.headers['API-KEY']
    if api_key == APP_CONFIG['yl_client']['api_key']
      boat = SunsailBoat.where(:supplier_guid => params['boat_guid']).first
      data = {}
      if boat.present?
        params['avail_boat']['boat_guid'] = params['boat_guid']
        params['avail_boat']['period_start_date'] = params['avail_boat']['period_start']
        params['avail_boat']['period_end_date'] = params['avail_boat']['period_end']
        puts params['avail_boat']
        boat.boat_prices.build params['avail_boat']
        boat.save ? data = {'avail_boat' => boat.boat_prices.last, 'status' => 'ok'} : data = {'status' => 'failure'}
      end
      respond_to do |format|
        format.json { render :json => data, :status => 200 }
      end
    else
      respond_to do |format|
        format.json { render :json => {}, :status => 401 }
      end
    end
  end

  def update
    @price = BoatPrice.find(params[:id])

    respond_to do |format|
      if @price.update_attributes(params[:avail_boat])
        format.json { render :json =>  @price }
      else
        format.json { render json: @price.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @price = BoatPrice.find(params[:id])
    @price.destroy

    respond_to do |format|
      format.json { render :json =>  @price }
    end
  end
end
