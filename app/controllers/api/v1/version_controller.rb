class Api::V1::VersionController < ApplicationController
  def index
    api_key = request.headers["API-KEY"]
    if api_key == APP_CONFIG['yl_client']['api_key']
      respond_to do |format|
        format.json { render :json => {}, :status => 200 }
      end
    else
      respond_to do |format|
        format.json { render :json => {}, :status => 401 }
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json { render :json => {}, :status => 200 }
    end
  end
end