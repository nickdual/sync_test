#!/bin/env ruby
# encoding: utf-8

class Api::V1::CountriesController < ApplicationController
  def list
    puts request.headers["API-KEY"]
    api_key = request.headers["API-KEY"]
    if api_key == APP_CONFIG['yl_client']['api_key']
      countries = [
          {"isocode" => "0xfrance", "name" => %q{!@#!@$@#''8”678><>?^#$23}},
          {"isocode" => "0xantigua", "name" => "Antigua"},
          {"isocode" => "0xantigua", "name" => "Antigua"},
          {"isocode" => "0xantigua", "name" => "Antigua"},
          {"isocode" => "0xbelize", "name" => "Belize"},
          {"isocode" => "0xbelize", "name" => "Belize"},
          {"isocode" => "0xgreece", "name" => "Greece"},
          {"isocode" => "0xunited_states", "name" => "UNITED STATES"},
          {"isocode" => "0xcuba", "name" => "Cuba"},
          {"isocode" => "0xcanaries", "name" => "Canaries"},
      ]
      respond_to do |format|
        format.json { render :json => {:countries => countries}, :status => 200 }
      end
    else
      respond_to do |format|
        format.json { render :json => {}, :status => 401 }
      end
    end
  end
end
