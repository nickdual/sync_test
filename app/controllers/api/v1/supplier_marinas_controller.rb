class Api::V1::SupplierMarinasController < ApplicationController
  def create
    api_key = request.headers["API-KEY"]
    if api_key == APP_CONFIG['yl_client']['api_key']
      marina = SupplierMarina.where(:supplier_guid => params["supplier_marina"]["supplier_guid"]).first
      data = {}
      if marina.blank?
        marina = SupplierMarina.new(params["supplier_marina"])
        marina.save ? data = {"supplier_marina" => marina, "status" => "ok"} : data = {"status" => "failure"}
      else
        data = {"supplier_marina" => marina, "status" => "exist"}
      end
      respond_to do |format|
        format.json { render :json => data, :status => 200 }
      end
    else
      respond_to do |format|
        format.json { render :json => {}, :status => 401 }
      end
    end
  end

  def list
    api_key = request.headers["API-KEY"]
    if api_key == APP_CONFIG['yl_client']['api_key']
      respond_to do |format|
        format.json { render :json => {:marinas => SupplierMarina.all}, :status => 200 }
      end
    else
      respond_to do |format|
        format.json { render :json => {}, :status => 401 }
      end
    end
  end
end