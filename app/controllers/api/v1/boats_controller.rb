class Api::V1::BoatsController < ApplicationController
  def create
    api_key = request.headers["API-KEY"]
    if api_key == APP_CONFIG['yl_client']['api_key']
      boat = SunsailBoat.where(:supplier_guid => params["boat"]["supplier_guid"]).first
      data = {}
      if boat.blank?
        boat = SunsailBoat.new(params["boat"])
        boat.save ? data = {"boat" => boat, "status" => "ok"} : data = {"status" => "failure"}
      else
        data = {"boat" => boat, "status" => "exist"}
      end
      respond_to do |format|
        format.json { render :json => data, :status => 200 }
      end
    else
      respond_to do |format|
        format.json { render :json => {}, :status => 401 }
      end
    end
  end

  def list
    api_key = request.headers["API-KEY"]
    if api_key == APP_CONFIG['yl_client']['api_key'] && params[:principal_guid] == 'tuisunsail'
      respond_to do |format|
        format.json { render :json => {:boats => SunsailBoat.all}, :status => 200 }
      end
    else
      respond_to do |format|
        format.json { render :json => {}, :status => 401 }
      end
    end
  end
end
