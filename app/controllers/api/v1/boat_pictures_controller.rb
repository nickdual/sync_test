class Api::V1::BoatPicturesController < ApplicationController
  def create
    api_key = request.headers["API-KEY"]
    if api_key == APP_CONFIG['yl_client']['api_key']
      boat = SunsailBoat.where(:supplier_guid => params["boat_id"]).first
      data = {}
      if boat.present?
        boat.boat_pictures.build params["picture"]
        boat.save ? data = {"boat_picture" => boat.boat_pictures.last, "status" => "ok"} : data = {"status" => "failure"}
      end
      respond_to do |format|
        format.json { render :json => data, :status => 200 }
      end
    else
      respond_to do |format|
        format.json { render :json => {}, :status => 401 }
      end
    end
  end

end