require 'json'

class YlSupplierMarina
  def self.list()
    client = YlClient.new
    response = client.get('/supplier_marinas/list')
    return JSON.parse(response.body)['marinas']
  end

  def self.create(supplier_guid, name, address, country)
    puts supplier_guid
    client = YlClient.new
    params = {
      :supplier_marina => {
        :supplier_guid => supplier_guid,
        :name => name,
        :address => address,
        :country => country
      }
    }
    response = client.post('/supplier_marinas', params)
    return JSON.parse(response.body)
  end

  def self.create_xls()
    #index by supplier guid
    response_marinas_raw = YlSupplierMarina.list()
    response_marinas = {}
    response_marinas_raw.each do |marina|
      response_marinas[marina["supplier_guid"]] = marina
    end

    #parser from supplier_maria.xls
    data = Excel.new(Rails.root.to_s + '/public/suppiler_marina.xls')
    local_marinas = ApplicationHelper.xls_to_json(data, [0, 5, 7, 22])
    local_marinas.each do |marina|
      if marina["BLLOCT"].present? && marina["BLNAME"].present? && marina["BLPORT"].present? && marina["BLCTRY"].present? && response_marinas[marina["BLLOCT"]].blank?
        response_marinas_raw.push(YlSupplierMarina.create(marina["BLLOCT"], marina["BLNAME"], marina["BLPORT"], marina["BLCTRY"])['supplier_marina'])
      end
    end
    return response_marinas_raw
  end
end
