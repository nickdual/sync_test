class SunsailBoat < ActiveRecord::Base
  attr_accessible :supplier_guid, :name, :model_name, :model_year, :description, :hull, :length_ft, :cabins, :berths, :heads, :max_persons, :fuel_capacity, :water_capacity

  has_many :boat_pictures, :class_name => 'BoatPicture', :foreign_key => :boat_id
  has_many :boat_prices, :class_name => 'BoatPrice', :foreign_key => :boat_id
end