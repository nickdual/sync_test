class SupplierMarina  < ActiveRecord::Base
  attr_accessible :supplier_guid, :name, :longitude, :latitude, :address
end