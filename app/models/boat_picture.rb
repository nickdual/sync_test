class BoatPicture < ActiveRecord::Base
  attr_accessible :width, :height, :url, :description

  belongs_to :sunsail_boat, :class_name => "SunsailBoat", :foreign_key => :boat_id
end