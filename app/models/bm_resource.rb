require "rexml/document"
include REXML

class BmResource < BmObject
	attr_accessor :name, :base, :principalguid, :berths, :year, :model,
                :length_ft, :cabins, :heads, :water_capacity, :fuel_capacity

  attr_accessor :images     # [Array<{:url, :description, :weight, :height}>]
  attr_accessor :prices     # [Array<BmPrice>]
  attr_accessor :blocked    # [Array<Period>]
  attr_accessor :extras     # [Array<BmExtra>]
  attr_accessor :discounts  # [Array<BmDiscount>]
  attr_accessor :equipment  # [Array<{:description, :value}>]

  @@resources = {}

	def sync
    client = YlClient.new

    boat = YlBoat.new(
        :name => name, :supplier_guid => supplier_guid, :model_name => model,
        :model_year => year, :description => '', :hull => 'mono',
        :length_ft => length_ft, :cabins => cabins, :berths => berths,
        :heads => heads, :max_persons => berths, :fuel_capacity => fuel_capacity,
        :water_capacity => water_capacity
    )
    
    principal_guid = 'bm' + @principalguid
    supplier_marina_guid = 'bm' + base
		YlBoat.create(boat, principal_guid, supplier_marina_guid)

		#Rails.logger.info images.to_json

		images.each do |i|
			response = client.post '/boat_pictures', {
          :boat_guid => 'bm' + id, :picture => i
      }
		end

		equipment.each do |i|
			response = client.post '/boat_attributes', {
          :boat_guid => 'bm' + id, :attribute => i
      }
		end
	end

  class << self

    def resources(company_id)
      return @@resources[company_id] if not @@resources[company_id].nil?

      client = BmClient.new
      resource_string = client.get_resources(company_id)
      @@resources[company_id] = self._parse_resources(resource_string, company_id)
      return @@resources[company_id]
    end

    # List boat by company id to console
    def list(company_id)
      resources = self.resources(company_id)
      resources.each do |r|
        Rails.logger.info r.name + ", " + r.model + ", " + r.base + ", " + r.id
      end
    end

    # list blocked availability for the given company id and boat id
    def list_booked_period(company_id, boat_id=nil)
      client = BmClient.new
      response_string = client.get_availability_info(company_id, 2013, true, '1970-01-01T00:00:01Z')
      doc = Document.new response_string

      boats = BmResource.resources(company_id)
      boats.each do |b|
        if boat_id.nil? or boat_id == b.id
          Rails.logger.info "Looking for booked periods for " + b.name + " " + b.id
          XPath.each(doc, "/root/reservation[@resourceid='" + b.id + "']") do |rv|
            Rails.logger.info " Booked: " + rv.attributes["datefrom"] + " " + rv.attributes["dateto"]
          end
        end
      end
    end

    def sync(company_id=nil, boat_id=nil)
      if company_id.nil?
        company_ids = APP_CONFIG['company_ids']
      else
        company_ids = [company_id]
      end
      company_ids.collect! {|x| x.to_s}

      company_ids.each do |company_id|
        resources = self.resources(company_id)

        # popular boat_guids for checking duplicate
        boats = YlBoat.list
        boat_guids = boats.collect {|x| x.supplier_guid}

        resources.each do |r|
          # check duplicate
          next if boat_guids.include?(r.supplier_guid)

          Rails.logger.info "Found boat " + r.id + " from company " + company_id + " and sending to YL."
          r.sync
        end
      end
    end

    # sync all prices for company listed in config file
    def sync_prices(company_id=nil, boat_id=nil)
      if company_id.nil?
        company_ids = APP_CONFIG['company_ids']
      else
        company_ids = [company_id]
      end
      company_ids.collect! {|x| x.to_s}

      company_ids.each do |company_id|
        resources = self.resources(company_id)

        resources.each do|resource|
          next if !boat_id.nil? and resource.id.to_s != boat_id.to_s

          resource.prices.each do |p|
            #total_extra = BmExtra.compute_mandatory_extras(p, resource.extras)
            #total_discount = BmDiscount.compute_discounts(p, resource.discounts)
            #Rails.logger.info "Base price, total extra, total discount: #{p.price}, #{total_extra}, #{total_discount}"
            #p.price = p.price + total_extra - total_discount
            #
            p.sync
          end
        end
      end
    end

    def _parse_resources(resource_string, company_id)
      doc = Document.new resource_string

      resources = []

      # parse resource
      doc.elements.each("root/resource") do |e|
        water = e.attributes["watercapacity"] =~ /(\d*)\./ ? $1 : nil
        fuel = e.attributes["fuelcapacity"] =~ /(\d*)\./ ? $1 : nil
        length = e.attributes["length"].to_f
        length_ft = meters_to_feet(length).ceil
        
        #incorrect_length = e.attributes["length"].to_i
        #incorrect_length_ft = meters_to_feet(incorrect_length)
        # puts "LENGTH #{e.attributes["model"]} -> #{length_ft} (#{e.attributes["length"]} #{length}) (was: #{incorrect_length_ft})" if 

        resource = BmResource.new(
            :name => e.attributes["name"],
            :id => e.attributes["id"],
            :base => e.attributes["base"],
            :model => e.attributes["model"],
            :principalguid => company_id,
            :berths => e.attributes["berths"].to_i,
            :year => e.attributes["year"],
            :length_ft => length_ft,
            :cabins => e.attributes["cabins"],
            :heads => e.attributes["heads"],
            :water_capacity => water,
            :fuel_capacity => fuel
        )

        images = []
        e.elements.each("images/image") do |i|
          images.push(
              {
               :url => i.attributes["href"],
               :description => i.attributes["comment"],
               :width => 0,
               :height => 0
              }
          )
        end

        equipment = []
        e.elements.each("equipment/equipmentitem") do |i|
          equipment.push(
              {
               :description => i.attributes["name"],
               :value => i.attributes["value"],
              }
          )
        end
        
        Rails.logger.info "\n\nWorking on boat " + resource.name

        prices = []
        e.elements.each("prices/price") do |p|
          price = BmPrice.new(boat=resource, from=p.attributes['datefrom'], to=p.attributes['dateto'], price=p.attributes['price'])
          prices.push(price)
        end
        prices.sort_by! {|p| [p.from, p.to] }

        # extract extras
        extras = []
        e.elements.each("extras/extra") do |ex|
          extra = BmExtra.parse(ex)
          extra.boat = resource
          extras.push(extra)
        end

        # extract discount
        discounts = []
        e.elements.each("discounts/discount") do |d|
          discount = BmDiscount.parse(d)
          discount.boat = resource
          discounts.push(discount)
        end

        resource.images = images
        resource.prices = prices
        resource.extras = extras
        resource.discounts = discounts
        resource.equipment = equipment
        
        resources.push(resource)
      end

      self._set_blocked(company_id, resources)

      return resources
    end

    ###
    # @param [String] company_id
    # @param [Array<BmResource>] boats
    # @return boats
    ###
    def _set_blocked(company_id, boats=nil)
      if boats.nil?
        boats = BmResource.resources(company_id)
      end

      client = BmClient.new
      doc = Document.new client.get_availability_info(company_id, 2013, true, '1970-01-01T00:00:01Z')

      boats.each do |b|
        blk = []
        XPath.each(doc, "/root/reservation[@resourceid='" + b.id + "']") do |rv|
          period_start = Date.parse(rv.attributes['datefrom'])
          period_end = Date.parse(rv.attributes['dateto'])
          period = Period.new(period_start, period_end)
          blk.push(period)
        end

        b.blocked = blk
      end

      return boats
    end

  end

end

def meters_to_feet(length)
  # 1 meter = 3.2808399 feet
  return length * 3.2808399
end
