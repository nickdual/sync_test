
class YlClient
  def initialize
    @api_url = APP_CONFIG['yl_client']['api_url']
    @api_key = APP_CONFIG['yl_client']['api_key']
    @headers = {'API-Key' => @api_key}
  end

  def version
    return Faraday.get @api_url + '/version', @headers
  end

  def get(endpoint)
    return Faraday.get @api_url + endpoint, @headers
  end

  def post(endpoint, params={})
    return Faraday.post @api_url + endpoint, params, @headers
  end

  def put(endpoint, params={})
    return Faraday.put @api_url + endpoint, params, @headers
  end

  def delete(endpoint)
    return Faraday.delete @api_url + endpoint, @headers
  end
end
