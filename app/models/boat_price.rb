class BoatPrice < ActiveRecord::Base
  attr_accessible :id, :boat_id, :boat_guid, :period_price, :period_nights, :period_start_date, :period_end_date, :calc

  belongs_to :sunsail_boat, :class_name => "SunsailBoat", :foreign_key => :boat_id
end