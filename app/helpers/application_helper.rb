module ApplicationHelper

	def	self.api_version
		Rails.logger.info "starting version call"
		response = Faraday.get 'http://127.0.0.1:3000/api/v1/version', 'API-Key' => '003e43a433f27d2597627edda018c98e'
		Rails.logger.info response.body
		Rails.logger.info "end version"
	end

	#def	marina
	#	Rails.logger.info "starting marina call"
	#	response = Faraday.post 'http://127.0.0.1:3000/api/v1/supplier_marina', { :marina => { :name => "Maguro" } }, 'API-Key' => '003e43a433f27d2597627edda018c98e'
	#	Rails.logger.info response.body
	#	Rails.logger.info "end marina"
	#end

  def self.xls_to_json(sheet, cols=nil)
    header = sheet.row(1)
    results = []
    (2..sheet.last_row).each do |iRow|
      row = Hash[[header, sheet.row(iRow)].transpose]
      if cols.present?
        i = 0
        row_compact = {}
        row.each do |index, value|
          row_compact[index] = value if cols.include?(i)
          i = i + 1
        end
        row = row_compact
      end
      results.push(row)
    end
    return results
  end
end
