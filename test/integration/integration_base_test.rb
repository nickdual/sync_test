require 'test_helper'

class IntegrationBaseTest < ActiveSupport::TestCase
  @@client = YlClient.new

  protected

  def check_environment
    begin
      response = @@client.get('/environment')
      rv = JSON.parse(response.body)
      Rails.logger.debug 'Environment: ' + rv['environment']
      assert rv['environment'] != 'production', 'YLSite is running on production environment. Please make sure it runs on `development` or `test` production.'
    rescue Exception => e
      assert false, 'YL site is down'
    end
  end
end