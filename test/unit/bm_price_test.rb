require 'test_helper'

class BmPriceTest < ActiveSupport::TestCase
  def setup
    @boat = BmResource.new(
        :name => 'Clara',
        :id => 1,
        :base => 'Venice',
        :model => 'Cruiser',
        :principalguid => 1,
        :berths => 50,
        :year => 2000,
        :length_ft => 5,
        :cabins => 3,
        :heads => 50,
        :water_capacity => 100,
        :fuel_capacity => 100
    )
    @price = BmPrice.new(@boat, (Date.today-360).to_s, (Date.today+360).to_s, 100)
  end

  def teardown
    @boat = nil
    @price = nil
  end

  test "price periods should start on saturdays" do
    assert_equal Date.parse('2012-10-06'), @price.closest_upcoming_saturday(Date.parse('2012-10-06'))
    assert_equal Date.parse('2012-10-06'), @price.closest_upcoming_saturday(Date.parse('2012-10-05'))
    assert_equal Date.parse('2012-10-06'), @price.closest_upcoming_saturday(Date.parse('2012-09-30'))
  end

  test "is blocked -- true" do
    @boat.blocked = []
    @boat.blocked.push(Period.new(Date.today, Date.today + 21))
    @boat.blocked.push(Period.new(Date.today - 14, Date.today - 7))

    assert @price.is_blocked?(Date.today-8, Date.today+1)
    assert @price.is_blocked?(Date.today, Date.today+21)
    assert @price.is_blocked?(Date.today-8, Date.today-5)
    assert @price.is_blocked?(Date.today-3, Date.today+1)
  end

  test "is blocked -- false" do
    @boat.blocked = []
    @boat.blocked.push(Period.new(Date.today, Date.today + 21))
    @boat.blocked.push(Period.new(Date.today - 14, Date.today - 7))

    assert !@price.is_blocked?(Date.today-7, Date.today)
    assert !@price.is_blocked?(Date.today-5, Date.today-1)
    assert !@price.is_blocked?(Date.today+21, Date.today + 28)
    assert !@price.is_blocked?(Date.today+22, Date.today + 29)
    assert !@price.is_blocked?(Date.today-21, Date.today-14)
    assert !@price.is_blocked?(Date.today-22, Date.today-15)
  end
end