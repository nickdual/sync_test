
class PeriodTest < ActiveSupport::TestCase
  def setup
    @period = Period.new(Date.today, Date.today+7)
  end

  def teardown
    @period = nil
  end

  test "doesn't overlap basic" do
    p1 = Period.new(Date.today+8, Date.today+9)
    p2 = Period.new(Date.today-7, Date.today-1)

    assert !@period.overlap?(p1), 'period should not overlap'
    assert !@period.overlap?(p2), 'period should not overlap'
  end

  test "doesn't overlap corner case" do
    p1 = Period.new(Date.today+7, Date.today + 8)
    p2 = Period.new(Date.today-1, Date.today)

    assert !@period.overlap?(p1), 'period should not overlap'
    assert !@period.overlap?(p2), 'period should not overlap'
  end

  test "overlap basic" do
    # p1 is inside @period
    p1 = Period.new(Date.today+1, Date.today+6)
    # p2 enclose @period
    p2 = Period.new(Date.today-1, Date.today+8)
    # the two period overlaps 1
    p3 = Period.new(Date.today-3, Date.today+4)
    # the two period overlaps 2
    p4 = Period.new(Date.today+3, Date.today+10)

    assert @period.overlap?(p1), 'period should overlap'
    assert @period.overlap?(p2), 'period should overlap'
    assert @period.overlap?(p3), 'period should overlap'
    assert @period.overlap?(p4), 'period should overlap'
  end

  test "overlap corner case" do
    # exactly the same period
    p1 = Period.new(Date.today, Date.today+7)
    # adjacent period, one day overlap
    p2 = Period.new(Date.today-1, Date.today+1)
    # adjacent period, one day overlap
    p3 = Period.new(Date.today+6, Date.today+7)

    assert @period.overlap?(p1), 'period should overlap'
    assert @period.overlap?(p2), 'period should overlap'
    assert @period.overlap?(p3), 'period should overlap'
  end
end