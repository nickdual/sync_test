require 'test_helper'

class BmBaseTest < ActiveSupport::TestCase
  def setup
    @base1 = BmBase.new(:id => '1', :name => 'Base1', :city => 'Jakarta', :country => 'Indonesia', :address => 'Menteng')
    @base2 = BmBase.new(:id => '2', :name => 'Base2', :city => 'Surabaya', :country => 'Indonesia', :address => 'Malang')
    @base3 = BmBase.new(:id => '3', :name => 'Base3', :city => 'Jawa Barat', :country => 'Indonesia', :address => 'Bandung')
  end

  def teardown
  end
  
  test "parse response" do
    response_string = <<eos
<root>
  <base id="#{@base1.id}" name="#{@base1.name}" city="#{@base1.city}" country="#{@base1.country}" address="#{@base1.address}"></base>
  <base id="#{@base2.id}" name="#{@base2.name}" city="#{@base2.city}" country="#{@base2.country}" address="#{@base2.address}"></base>
  <base id="#{@base3.id}" name="#{@base3.name}" city="#{@base3.city}" country="#{@base3.country}" address="#{@base3.address}"></base>
</root>
eos

    bm_bases = BmBase._parse_response(response_string)

    assert_equal 3, bm_bases.length
    assert_equal @base1, bm_bases[0]
    assert_equal @base2, bm_bases[1]
    assert_equal @base3, bm_bases[2]
  end
end
