require 'test_helper'

class BmClientTest < ActiveSupport::TestCase
  test "request" do
    client = BmClient.new
    response = client.request('getBases')

    assert !response.blank?, 'response is blank'
    assert !response.body.blank?, 'response body is blank'
  end
end
