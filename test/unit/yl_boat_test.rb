require 'test_helper'
require 'json'

require File.dirname(__FILE__) + '/yl_base_test.rb'

class YlBoatTest < YlBaseTest
  test "list" do
    check_yl_site_is_up

    # shouldn't throw any exception
    boats = YlBoat.list
  end

  test "list by boat guid - arbitrary boat guid" do
    check_yl_site_is_up

    # shouldn't throw any exception
    boats = YlBoat.list_by_boat_guid('bm1')
  end

  test "list by principal guid - arbitrary principal guid" do
    check_yl_site_is_up

    # shouldn't throw any exception
    boats = YlBoat.list_by_principal_guid('bm1')
  end

  test "parse json" do
    boat1_hash = {
      id: 1,
      supplier_guid: 'navigare_denis',
      name: 'Denis',
      model_name: 'Bavaria 38 Cruiser',
      model_year: 2009,
      description: 'This is a great boat',
      hull: 'mono',
      length_ft: 38,
      cabins: 3,
      berths: 6,
      heads: 1,
      max_persons: 6,
      fuel_capacity: 200,
      water_capacity: 150
    }

    boat2_hash = {
      id: 2,
      supplier_guid: 'navigare_alice',
      name: 'Alice',
      model_name: 'Bavaria 38 Cruiser',
      model_year: 2009,
      description: 'This is a great boat',
      hull: 'mono',
      length_ft: 38,
      cabins: 3,
      berths: 6,
      heads: 1,
      max_persons: 6,
      fuel_capacity: 200,
      water_capacity: 150
    }

    json_hash = { :boats => [] }
    json_hash[:boats].push(boat1_hash)
    json_hash[:boats].push(boat2_hash)

    json_string = json_hash.to_json

    boats = YlBoat._parse_json(json_string)

    assert_equal(2, boats.length)
    # compare two boats
    boats.each_with_index do |boat, i|
      boat_hash = json_hash[:boats][i]
      boat_hash.keys.each do |key|
        assert_equal(boat_hash[key], boat.send(key.to_s))
      end
    end
  end
end
