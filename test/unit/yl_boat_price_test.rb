require 'test_helper'

require File.dirname(__FILE__) + '/yl_base_test.rb'

class YlBoatPriceTest < YlBaseTest
  test "list" do
    check_yl_site_is_up
    
    # shouldn't throw any exception
    boats = YlBoatPrice.list
  end

  test "list by boat guid -- arbitrary boat guid" do
    check_yl_site_is_up
    
    # shouldn't throw any exception
    boats = YlBoatPrice.list_by_boat_guid('bm1')
  end

  test "parse json boats" do
    json_hash = {
      :boats =>
      [
       {
        :boat_id => 1,
        :boat_avail_prices =>
        [
         {
           :id => 1,
           :period_price => 50,
           :period_nights => 7,
           :period_start_date => "2012-03-01",
           :period_end_date => "2012-03-08"
         },
         {
           :id => 2,
           :period_price => 150,
           :period_nights => 7,
           :period_start_date => "2012-03-08",
           :period_end_date => "2012-03-15"
         }
        ]
       },
       {
        :boat_id => 2,
        :boat_avail_prices =>
        [
         {
           :id => 1,
           :period_price => 50,
           :period_nights => 7,
           :period_start_date => "2012-03-01",
           :period_end_date => "2012-03-08"
         },
         {
           :id => 2,
           :period_price => 150,
           :period_nights => 7,
           :period_start_date => "2012-03-08",
           :period_end_date => "2012-03-15"
         }
        ]
       },
      ]
    }
    
    json_string = json_hash.to_json

    prices = YlBoatPrice._parse_json(json_string)
    assert_equal(4, prices.length)
    prices.each_with_index do |price, i|
      price_hash = json_hash[:boats][i/2][:boat_avail_prices][i%2]
      keys = price_hash.keys.reject{|key| [:period_start_date, :period_end_date].include?(key)}
      keys.each do |key|
        assert_equal(price_hash[key], price.send(key.to_s))
      end
    end
  end

  test "parse json boat" do
    json_hash = {
      :boat_id => 1,
      :boat_avail_prices =>
      [
       {
         :id => 1,
         :period_price => 50,
         :period_nights => 7,
         :period_start_date => "2012-03-01",
         :period_end_date => "2012-03-08"
       },
       {
         :id => 2,
         :period_price => 150,
         :period_nights => 7,
         :period_start_date => "2012-03-08",
         :period_end_date => "2012-03-15"
       }
      ]
    }

    json_string = json_hash.to_json

    prices = YlBoatPrice._parse_json(json_string)
    assert_equal(2, prices.length)
    # compare prices
    prices.each_with_index do |price, i|
      price_hash = json_hash[:boat_avail_prices][i]
      keys = price_hash.keys.reject{|key| [:period_start_date, :period_end_date].include?(key)}
      keys.each do |key|
        assert_equal(price_hash[key], price.send(key.to_s))
      end
    end
  end
end
