require 'test_helper'

class BmExtraTest < ActiveSupport::TestCase
  def setup
    @boat = BmResource.new(
      :name => 'boat1',
      :id => '123',
      :base => 'vienna',
      :berths => 5
    )
    @epoch = Date.parse("1970-1-1")
    @price = YlBoatPrice.new(
        :period_price => 1000,
        :period_nights => 7,
        :period_start => Date.today,
        :period_end => Date.today + 7
    )
  end

  def teardown
    @boat = nil
    @epoch = nil
    @price = nil
  end

  test "multiple extras applied, per person in name" do
    extras = []
    # should apply
    extra1 = BmExtra.new(
      :name => 'xxx outboard engine xxx',
      :price => 1,
      :boat => @boat,
      :valid_date_from => @epoch,
      :valid_date_to => Date.today + 1.year
    )
    extras.push(extra1)

    # should apply
    extra2 = BmExtra.new(
      :name => 'comfort per person',
      :price => 1,
      :boat => @boat,
      :valid_date_from => @epoch,
      :valid_date_to => Date.today + 1.year
    )
    extras.push(extra2)

    # should not apply
    extra3 = BmExtra.new(
      :name => 'xxx',
      :obligatory => false,
      :price => 1,
      :boat => @boat,
      :valid_date_from => @epoch,
      :valid_date_to => Date.today + 1.year
    )
    extras.push(extra3)

    # test
    t_extra = extra1.price + extra2.price * @boat.berths
    extra = BmExtra.compute_mandatory_extras(@price, extras)
    assert_equal(t_extra, extra)
  end

  test "already included in base price should not be included" do
    extras = []
    extra1 = BmExtra.new(
      :name => '1',
      :price => 1,
      :included_in_base_price => true,
      :obligatory => true,
      :boat => @boat,
      :valid_date_from => @epoch,
      :valid_date_to => Date.today + 1.year
    )
    extras.push(extra1)

    extra = BmExtra.compute_mandatory_extras(@price, extras)

    assert_equal(0, extra)
  end

  test "not obligatory should not be included" do
    extras = []
    extra1 = BmExtra.new(
        :name => '1',
        :price => 1,
        :included_in_base_price => false,
        :obligatory => false,
        :boat => @boat,
        :valid_date_from => @epoch,
        :valid_date_to => Date.today + 1.year
    )
    extras.push(extra1)

    extra = BmExtra.compute_mandatory_extras(@price, extras)

    assert_equal(0, extra)
  end

  test "extra which contains special string should be included" do
    extras = []
    extra1 = BmExtra.new(
        :name => '1 outboard engine',
        :price => 1,
        :boat => @boat,
        :valid_date_from => @epoch,
        :valid_date_to => Date.today + 1.year
    )
    extra2 = BmExtra.new(
        :name => '2 comfort',
        :price => 2,
        :boat => @boat,
        :valid_date_from => @epoch,
        :valid_date_to => Date.today + 1.year
    )
    extra3 = BmExtra.new(
        :name => '3 cleaning',
        :price => 3,
        :boat => @boat,
        :valid_date_from => @epoch,
        :valid_date_to => Date.today + 1.year
    )
    extra4 = BmExtra.new(
        :name => '4 tourist tax',
        :price => 4,
        :boat => @boat,
        :valid_date_from => @epoch,
        :valid_date_to => Date.today + 1.year
    )

    extras.push(extra1)
    extras.push(extra2)
    extras.push(extra3)
    extras.push(extra4)

    t_extra = extra1.price + extra2.price + extra3.price + extra4.price
    extra = BmExtra.compute_mandatory_extras(@price, extras)

    assert_equal(t_extra, extra)
  end

  test "time unit" do
    extras = []

    extra1 = BmExtra.new(
      :name => '1',
      :price => 1,
      :included_in_base_price => false,
      :obligatory => true,
      :time_unit => 0,
      :boat => @boat,
      :valid_date_from => @epoch,
      :valid_date_to => Date.today + 1.year
    )
    extra2 = BmExtra.new(
        :name => '2',
        :price => 2,
        :included_in_base_price => false,
        :obligatory => true,
        :time_unit => 1,
        :boat => @boat,
        :valid_date_from => @epoch,
        :valid_date_to => Date.today + 1.year
    )
    extra3 = BmExtra.new(
        :name => '3',
        :price => 3,
        :included_in_base_price => false,
        :obligatory => true,
        :time_unit => 7,
        :boat => @boat,
        :valid_date_from => @epoch,
        :valid_date_to => Date.today + 1.year
    )

    extras.push(extra1)
    extras.push(extra2)
    extras.push(extra3)

    t_extra = extra1.price + extra2.price * 7 + extra3.price
    extra = BmExtra.compute_mandatory_extras(@price, extras)

    assert_equal(t_extra, extra)
  end

  test "match per person" do
    extras = []
    # should apply
    extra1 = BmExtra.new(
      :name => '1 per person',
      :price => 1,
      :included_in_base_price => false,
      :obligatory => true,
      :boat => @boat,
      :valid_date_from => @epoch,
      :valid_date_to => Date.today + 1.year
    )
    # should apply
    extra2 = BmExtra.new(
      :name => '2',
      :price => 2,
      :per_person => true,
      :included_in_base_price => false,
      :obligatory => true,
      :boat => @boat,
      :valid_date_from => @epoch,
      :valid_date_to => Date.today + 1.year
    )
    # should not apply
    extra3 = BmExtra.new(
        :name => '3',
        :price => 3,
        :per_person => true,
        :included_in_base_price => true,
        :obligatory => true,
        :boat => @boat,
        :valid_date_from => @epoch,
        :valid_date_to => Date.today + 1.year
    )
    # should not apply
    extra4 = BmExtra.new(
        :name => '4',
        :price => 4,
        :per_person => true,
        :included_in_base_price => false,
        :obligatory => false,
        :boat => @boat,
        :valid_date_from => @epoch,
        :valid_date_to => Date.today + 1.year
    )

    extras.push(extra1)
    extras.push(extra2)
    extras.push(extra3)
    extras.push(extra4)

    t_extra = extra1.price * @boat.berths + extra2.price * @boat.berths

    extra = BmExtra.compute_mandatory_extras(@price, extras)

    assert_equal(t_extra, extra)
  end
end