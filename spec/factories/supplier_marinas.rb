FactoryGirl.define do
  factory :marinaOne, :class => 'SupplierMarina' do  |f|
    f.supplier_guid "shaton"
    f.name "Songoku"
    f.longitude 1.345345234
    f.latitude 322.234234234
    f.address "Earth"
  end
  factory :marinaTwo, :class => "SupplierMarina" do  |f|
    f.supplier_guid "zeus"
    f.name "Buu"
    f.longitude 12.4334322
    f.latitude 234.233453
    f.address "Unspecific"
  end
end