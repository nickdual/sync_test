require 'spec_helper'

describe YlSupplierMarina do
  describe 'list' do
    before(:all) do
      FactoryGirl.create(:marinaOne)
      FactoryGirl.create(:marinaTwo)
    end
    it "with paramater" do
      YlSupplierMarina.list(SupplierMarina.all).should_not == nil
    end
    it "without paramater" do
      YlSupplierMarina.list().should_not == nil
    end
  end
  #need run server before run this test
  describe 'create' do
    it "data exist" do
      @marinaOne = FactoryGirl.create(:marinaOne)
      data = YlSupplierMarina.create(@marinaOne[:supplier_guid], @marinaOne[:name], @marinaOne[:address], @marinaOne[:country])
      data["status"].should == "exist"
    end
    it "noexist" do
      @marinaOne = FactoryGirl.create(:marinaOne)
      data = YlSupplierMarina.create((0...8).map{(65+rand(26)).chr}.join, "232", "3345", "464")
      data["status"].should == "ok"
    end
  end
end