require 'spec_helper'

describe Api::V1::SupplierMarinasController do
  describe "get list" do
    it 'with authentication' do
      request.env['API-KEY'] = '003e43a433f27d2597627edda018c98e'
      request.env['HTTP_ACCEPT'] = "application/json"
      response.status.should == 200 && response.body.should_not == nil
    end
    it "without authentication" do
      request.env['API-KEY'] = 'shaton'
      request.env['HTTP_ACCEPT'] = "application/json"
      get :list
      response.status.should eq 401
    end
  end

  describe "post create" do
    context "with authentication" do
      before(:all) do
        item = FactoryGirl.create(:marinaOne)
      end
      it "duplicate" do
        request.env['API-KEY'] = '003e43a433f27d2597627edda018c98e'
        request.env['HTTP_ACCEPT'] = "application/json"
        post :create, supplier_marina: FactoryGirl.attributes_for(:marinaOne), :format => :json
        data = ActiveSupport::JSON.decode(response.body)
        data["supplier_marina"].delete("id")
        data["status"].should == "exist" &&
            data["supplier_marina"].should == ActiveSupport::JSON.decode(FactoryGirl.attributes_for(:marinaOne).to_json)
      end
      it "non-duplicate" do
        request.env['API-KEY'] = '003e43a433f27d2597627edda018c98e'
        request.env['HTTP_ACCEPT'] = "application/json"
        post :create, supplier_marina: FactoryGirl.attributes_for(:marinaTwo), :format => :json
        data = ActiveSupport::JSON.decode(response.body)
        data["supplier_marina"].delete("id")
        data["status"].should == "ok" &&
            data["supplier_marina"].should == ActiveSupport::JSON.decode(FactoryGirl.attributes_for(:marinaTwo).to_json)
      end
    end
    it "request with authentication" do
      request.env['API-KEY'] = 'shaton'
      request.env['HTTP_ACCEPT'] = "application/json"
      post :create
      response.status.should eq 401
    end
  end

end